import logging 
from flask import Blueprint, render_template, request, send_from_directory, redirect, abort

from utils import is_filename_allowed 
from utils import update_posts

loader = Blueprint('loader', __name__, static_folder='../uploads')

logging.basicConfig(filename='base.log', level=logging.INFO)

UPLOAD_FOLDER = "uploads/images"

@loader.route('/post')
def page_post_form():
    return render_template('loader/post_form.html')



@loader.route('/post', methods=['POST'])
def page_post_upload():
    if request.method == 'POST':

        pic = request.files.get('picture')
        content = request.form.get('content')
        filename = pic.filename
        
        
        if not is_filename_allowed(filename):
            logging.exception('файл - не картинка')
            return render_template('loader/error.html')
        else:
            pic.save(f'./{UPLOAD_FOLDER}/{filename}')

        picture = f'http://127.0.0.1:5000/{UPLOAD_FOLDER}/{filename}'

        if not update_posts(pic=picture, content=content):
            logging.exception('ошибка обновления файла posts.json')
            return render_template('loader/error.html')
        else:
            return render_template('loader/post_uploaded.html', picture=picture, content=content)



@loader.route('/uploads/<path:path>')
def static_dir(path):
    return send_from_directory('uploads', path)
