import json
import logging

from pathlib import Path

from .load_data_json import load_data_json


logging.basicConfig(filename='basic.log', level=logging.INFO)

PATH = Path(__file__).parents[1]


def update_posts(pic, content):
    try:
        data = load_data_json()
        data.append(dict(pic=pic, content=content))

        with open(f'{PATH}/data/posts.json', 'w', encoding='UTF-8') as fl:
            json.dump(data, fl, ensure_ascii=False)
        return True
    except Exception:
        logging.exception('Не удалось записать изменение в файл')
