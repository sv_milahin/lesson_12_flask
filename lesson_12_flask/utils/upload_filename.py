ALLOWED_EXTENSIONS = {'jpeg', 'png'}


def is_filename_allowed(filename):
    extension = filename.split('.')[-1]

    if extension in ALLOWED_EXTENSIONS:
        return True
    return False
