import json
import logging
from pathlib import Path
from json import JSONDecodeError


logging.basicConfig(filename='basic.log')

PATH = Path(__file__).parents[1]



def load_data_json():
    try:
        with open(f'{PATH}/data/posts.json', encoding='UTF-8') as fl:
            data = json.load(fl)
        return data
    except FileNotFoundError:
        logging.exception('Файл не найден')
    except JSONDecodeError:
        logging.exception('Файл не удается преобразовать')


