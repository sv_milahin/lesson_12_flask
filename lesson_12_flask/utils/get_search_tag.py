from utils import load_data_json


def get_search_tag(s):
   data = load_data_json()
   return [content for content in data 
           if s.lower() in content['content'].lower()
           ]
