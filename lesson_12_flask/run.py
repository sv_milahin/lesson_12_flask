from dotenv import find_dotenv, load_dotenv

from app import app 

load_dotenv(find_dotenv('env'))

if __name__ == '__main__':
    app.run()
