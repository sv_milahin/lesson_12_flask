from utils.get_search_tag import get_search_tag


def test_get_search_tag():

    food = get_search_tag('еда')
    cat = get_search_tag('КОТ')

    assert food[0]['content'][:5] == 'Ага, ' 
    assert cat[0]['content'][:6] == 'Вышел '

