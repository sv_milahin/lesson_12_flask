import logging
from flask import Blueprint, render_template, request, redirect, url_for

from utils.get_search_tag import get_search_tag

logging.basicConfig(filename='base.log', level=logging.INFO)

main = Blueprint('main', __name__)


@main.route('/')
def index_page():
    return render_template('main/index.html')


@main.route('/search/')
def search_page():
    try:
        s = request.args.get('s')
        tags = get_search_tag(s) 
        return render_template('main/post_by_tag.html', s=s, tags=tags)
    except Exception:
        logging.exception('поиск не выполнен')
        return redirect(url_for('main.index_page'))
    

