from flask import Flask 

from main import main
from loader import loader


app = Flask(__name__, template_folder='../templates', static_folder='../static')

app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024

app.register_blueprint(main, url_prefix='/')
app.register_blueprint(loader, url_prefix='/')




